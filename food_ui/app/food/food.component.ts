import {Component} from 'angular2/core'
import {FoodService} from './food.service'

@Component({
    selector: 'food',
    templateUrl: 'app/food/food.template.html',
   	providers:[FoodService]
})
export class FoodComponent {
	food;

	constructor(foodService: FoodService){
		this.food = foodService.loadFoodList();
	}
}