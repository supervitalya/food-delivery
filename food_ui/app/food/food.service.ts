
export class FoodService{

	loadFoodList(){
		return [
			{ "title": "Pizza", "price": 200, "weight": 400 },
			{ "title": "Coca-cola", "price": 100, "weight": 500 },
			{ "title": "Beer", "price": 70, "weight": 1500 }
		];
	}
}