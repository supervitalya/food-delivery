System.register([], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var FoodService;
    return {
        setters:[],
        execute: function() {
            FoodService = (function () {
                function FoodService() {
                }
                FoodService.prototype.loadFoodList = function () {
                    return [
                        { "title": "Pizza", "price": 200, "weight": 400 },
                        { "title": "Coca-cola", "price": 100, "weight": 500 },
                        { "title": "Beer", "price": 70, "weight": 1500 }
                    ];
                };
                return FoodService;
            }());
            exports_1("FoodService", FoodService);
        }
    }
});
//# sourceMappingURL=food.service.js.map