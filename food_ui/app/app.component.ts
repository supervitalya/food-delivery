import {Component} from 'angular2/core';
import {FoodComponent} from './food/food.component';

@Component({
    selector: 'my-app',
    template: `
    <h1>Here is {{title}} - My First Angular 2 App. And my name is: {{name}}.</h1>
    <food></food>
    `,
    directives: [FoodComponent]
})
export class AppComponent {
	title='Food store';
	name = 'Vasya';
 }