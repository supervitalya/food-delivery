package com.epam.food.service;

import com.epam.food.domain.Order;

import java.util.List;

public interface OrderService extends CrudService<Order> {

    List<Order> findByuserId(Long userId);
}


