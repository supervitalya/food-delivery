package com.epam.food.service;

import com.epam.food.domain.Food;

public interface FoodService extends CrudService<Food> {
}
