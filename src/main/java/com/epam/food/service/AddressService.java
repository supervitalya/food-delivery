package com.epam.food.service;

import com.epam.food.domain.Address;

import java.util.List;

public interface AddressService extends CrudService<Address> {

    List<Address> findByUserId(Long userId);
}
