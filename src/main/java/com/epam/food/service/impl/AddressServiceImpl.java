package com.epam.food.service.impl;

import com.epam.food.domain.Address;
import com.epam.food.repository.AddressRepository;
import com.epam.food.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AddressServiceImpl implements AddressService {
    @Autowired
    private AddressRepository repository;

    @Override
    public AddressRepository getRepository() {
        return repository;
    }

    @Override
    public List<Address> findByUserId(Long userId) {
        return repository.findByUserId(userId);
    }
}
