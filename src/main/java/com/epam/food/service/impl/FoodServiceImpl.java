package com.epam.food.service.impl;

import com.epam.food.repository.FoodRepository;
import com.epam.food.service.FoodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FoodServiceImpl implements FoodService {
    @Autowired
    private FoodRepository repository;

    @Override
    public FoodRepository getRepository() {
        return repository;
    }
}
