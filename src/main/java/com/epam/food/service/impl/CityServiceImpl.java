package com.epam.food.service.impl;

import com.epam.food.repository.CityRepository;
import com.epam.food.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CityServiceImpl implements CityService {
    @Autowired
    private CityRepository repository;

    @Override
    public CityRepository getRepository() {
        return repository;
    }
}
