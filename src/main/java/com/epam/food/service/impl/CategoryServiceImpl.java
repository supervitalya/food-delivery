package com.epam.food.service.impl;

import com.epam.food.repository.CategoryRepository;
import com.epam.food.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CategoryServiceImpl implements CategoryService {
    @Autowired
    private CategoryRepository repository;

    @Override
    public CategoryRepository getRepository() {
        return repository;
    }
}
