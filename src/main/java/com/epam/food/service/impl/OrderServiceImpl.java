package com.epam.food.service.impl;

import com.epam.food.domain.Order;
import com.epam.food.repository.OrderRepository;
import com.epam.food.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {
    @Autowired
    private OrderRepository repository;

    @Override
    public OrderRepository getRepository() {
        return repository;
    }

    @Override
    public List<Order> findByuserId(Long userId) {
        return repository.findByUserId(userId);
    }
}
