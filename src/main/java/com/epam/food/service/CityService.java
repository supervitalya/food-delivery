package com.epam.food.service;

import com.epam.food.domain.City;

public interface CityService extends CrudService<City> {
}
