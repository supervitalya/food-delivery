package com.epam.food.service;

import com.epam.food.domain.Category;

public interface CategoryService extends CrudService<Category> {
}
