package com.epam.food.service;

import org.springframework.data.repository.CrudRepository;

import java.io.Serializable;
import java.util.List;

public interface CrudService<T extends Serializable> {

    CrudRepository<T, Long> getRepository();

    default T save(T entity) {
        return getRepository().save(entity);
    }

    default T findOne(Long id) {
        return getRepository().findOne(id);
    }

    default List<T> findAll() {
        return (List<T>) getRepository().findAll();
    }

    default void delete(Long id) {
        getRepository().delete(id);
    }

}
