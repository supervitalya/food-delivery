package com.epam.food.service;

import com.epam.food.domain.User;

public interface UserService extends CrudService<User> {
}
