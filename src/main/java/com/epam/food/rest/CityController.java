package com.epam.food.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.epam.food.domain.City;
import com.epam.food.service.CityService;

@RestController
@RequestMapping("/cities")
public class CityController {
	@Autowired
	private CityService cityService;

	@RequestMapping(method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public List<City> retrieveAll() {
		return cityService.findAll();
	}

	@RequestMapping(method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public City saveCity(@RequestBody City city) {
		return cityService.save(city);
	}

	@RequestMapping(value = "/{cityId}", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.OK)
	public City updateCity(@PathVariable Long cityId, @RequestBody City city) {
		return cityService.save(city);
	}

	@RequestMapping(value = "/{cityId}", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteCity(@PathVariable Long cityId) {
		cityService.delete(cityId);
	}
}
