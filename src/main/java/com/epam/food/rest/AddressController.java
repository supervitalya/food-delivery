package com.epam.food.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.epam.food.domain.Address;
import com.epam.food.service.AddressService;

@RestController
@RequestMapping("/addresses")
public class AddressController {

	@Autowired
	private AddressService addressService;

	@RequestMapping(value = "/user/{userId}", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public List<Address> retrieveAllByUserId(@PathVariable Long userId) {
		return addressService.findAll();
	}

	@RequestMapping(value = "/{addressId}")
	@ResponseStatus(HttpStatus.OK)
	public Address retrieveById(@PathVariable Long addressId) {
		return addressService.findOne(addressId);
	}

	@RequestMapping(method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public Address saveAddress(@RequestBody Address address) {
		return addressService.save(address);
	}

	@RequestMapping(value = "/{addressId}", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.OK)
	public Address updateAddress(@PathVariable Long addressId, @RequestBody Address address) {
		return addressService.save(address);
	}

	@RequestMapping(method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteAddress(@PathVariable Long addressId) {
		addressService.delete(addressId);
	}

}
