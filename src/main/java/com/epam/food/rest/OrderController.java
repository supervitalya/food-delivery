package com.epam.food.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.epam.food.domain.Order;
import com.epam.food.service.OrderService;

@RestController
@RequestMapping("/orders")
public class OrderController {
	@Autowired
	private OrderService orderService;

	@RequestMapping(value = "/user/{userId}", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public List<Order> retrieveOrdersByUserId(@PathVariable Long userId) {
		return orderService.findAll();
	}

	@RequestMapping(value = "{orderId}", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public Order retrieveById(@PathVariable Long orderId) {
		return orderService.findOne(orderId);
	}

	@RequestMapping(method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public Order saveOrder(@RequestBody Order order) {
		return orderService.save(order);
	}

	@RequestMapping(method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.OK)
	public Order updateOrder(@PathVariable Long orderId, @RequestBody Order order) {
		return orderService.save(order);
	}

	@RequestMapping(value = "/{orderId}", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteOrder(@PathVariable Long orderId) {
		orderService.delete(orderId);
	}
}
