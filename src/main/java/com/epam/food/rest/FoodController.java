package com.epam.food.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.epam.food.domain.Food;
import com.epam.food.service.FoodService;

@RestController
@RequestMapping("/food")
public class FoodController {
	@Autowired
	private FoodService foodService;

	@RequestMapping(method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public List<Food> retrieveFoodList() {
		return foodService.findAll();
	}

	@RequestMapping(value = "/{foodId}", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public Food retrieveFoodById(@PathVariable Long foodId) {
		return foodService.findOne(foodId);
	}

	@RequestMapping(method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public Food saveFood(@RequestBody Food food) {
		return foodService.save(food);
	}

	@RequestMapping(method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.OK)
	public Food updateFood(@RequestBody Food food) {
		return foodService.save(food);
	}

	@RequestMapping(value = "/{foodId}", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteFood(@PathVariable Long foodId) {
		foodService.delete(foodId);
	}
}
