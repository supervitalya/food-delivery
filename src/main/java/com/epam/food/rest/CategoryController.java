package com.epam.food.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.epam.food.domain.Category;
import com.epam.food.service.CategoryService;

@RestController
@RequestMapping("/categories")
public class CategoryController {
	@Autowired
	private CategoryService categoryService;

	@RequestMapping(method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public List<Category> retrieveAllCategories() {
		return categoryService.findAll();
	}

	@RequestMapping(method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public Category saveCategory(@RequestBody Category category) {
		return categoryService.save(category);
	}

	@RequestMapping(value = "/{categoryId}", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.OK)
	public Category updateCategory(@PathVariable Long categoryId, @RequestBody Category category) {
		return categoryService.save(category);
	}

	@RequestMapping(value = "/{categoryId}", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteCategory(@PathVariable Long categoryId) {
		categoryService.delete(categoryId);
	}
}
