package com.epam.food.domain.enums;

import java.util.Optional;
import java.util.stream.Stream;

public enum UserRoleEnum {
    USER(0), ADMIN(1);
    private int id;

    UserRoleEnum(int id) {
        this.id = id;
    }

    public Optional<UserRoleEnum> getById(int id) {
        return Stream.of(UserRoleEnum.values())
                .filter(e -> e.id == id)
                .findFirst();
    }
}
