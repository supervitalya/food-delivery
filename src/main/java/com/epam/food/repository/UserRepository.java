package com.epam.food.repository;

import org.springframework.data.repository.CrudRepository;

import com.epam.food.domain.User;

public interface UserRepository extends CrudRepository<User, Long> {

}
