package com.epam.food.repository;

import org.springframework.data.repository.CrudRepository;

import com.epam.food.domain.City;

public interface CityRepository extends CrudRepository<City, Long> {

}
