package com.epam.food.repository;

import org.springframework.data.repository.CrudRepository;

import com.epam.food.domain.Category;

public interface CategoryRepository extends CrudRepository<Category, Long> {

}
