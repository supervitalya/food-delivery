package com.epam.food.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import com.epam.food.domain.Food;

public interface FoodRepository extends CrudRepository<Food, Long> {

	List<Food> findByCategoryId(Long categoryId, Pageable pageable);

	List<Food> findAll(Pageable pageable);
}
