package com.epam.food.repository;

import org.springframework.data.repository.CrudRepository;

import com.epam.food.domain.Order;

import java.util.List;

public interface OrderRepository extends CrudRepository<Order, Long> {

    List<Order> findByUserId(Long userId);
}
