package com.epam.food.repository;

import org.springframework.data.repository.CrudRepository;

import com.epam.food.domain.Address;

import java.util.List;

public interface AddressRepository extends CrudRepository<Address, Long> {

    List<Address> findByUserId(Long userId);
}
